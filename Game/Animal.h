#ifndef ANIMAL_H
#define ANIMAL_H

#include "Object.h"
#include "Defs.h"

namespace Game {

class Cage;

class Animal : public Object
{

public:
    Animal();
    virtual ~Animal(){}

    int age() const { return _age; }
    int wellBeing() const { return _wellBeing; }
    int isFed() const { return _isFed; }

    Cage *cage() const { return _cage; }
    void setCage(Cage *c);

private:

    Age _age;
    int _wellBeing;
    bool _isFed;

    Cage* _cage;

};

}
#endif // ANIMAL_H
