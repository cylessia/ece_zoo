#ifndef LION_H
#define LION_H

#include "../Animal.h"

namespace Game {

class Lion : public Animal
{
public:

    Lion();

    void update(const Zoo &z);

private:
};

}
#endif // LION_H
