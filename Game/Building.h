#ifndef BUILDING_H
#define BUILDING_H

#include "Object.h"

namespace Game {

class Building : public Object
{
public:
    Building();
    virtual ~Building(){}
};

}

#endif // BUILDING_H
