#ifndef BUSINESSBUILDING_H
#define BUSINESSBUILDING_H

#include "Building.h"

namespace Game {

class BusinessBuilding : public Building
{
public:
    BusinessBuilding();
    virtual ~BusinessBuilding(){}
};

}

#endif // BUSINESSBUILDING_H
