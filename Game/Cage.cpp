#include "Cage.h"

#include <algorithm>

using namespace Game;

Cage::Cage()
{

}

void Cage::addAnimal(Animal *animal){
    _animals.push_back(animal);
}

void Cage::removeAnimal(Animal *animal){
    std::vector<Animal*>::iterator it = std::find(_animals.begin(), _animals.end(), animal);
    _animals.erase(it);
}
