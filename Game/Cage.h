#ifndef CAGE_H
#define CAGE_H

#include <vector>

#include "Object.h"

namespace Game {

class Animal;

class Cage : public Object
{
public:
    Cage();
    virtual ~Cage(){}

    std::vector<Animal*> animals() const { return _animals; }
    void addAnimal(Animal* animal);
    void removeAnimal(Animal* animal);

private:
    std::vector<Animal*> _animals;
};

}
#endif // CAGE_H
