#ifndef CARETAKER_H
#define CARETAKER_H

#include "Employee.h"

namespace Game {

class Caretaker : public Employee
{
public:
    Caretaker(std::string name, unsigned int exp, unsigned int hiredDay);
    virtual ~Caretaker(){}
};

}

#endif // CARETAKER_H
