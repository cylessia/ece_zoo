#include "Defs.h"

#include "Utils/Utils.h"

using namespace Game;

const unsigned int Definitions::Staff::MaxExperience = 300;
const int Definitions::Map::DefaultWidth = 100;
const int Definitions::Map::DefaultHeight = 100;

const std::vector<std::string> Definitions::_firstnames = {
    "Gabriel", "Raphael", "Leo", "Louis", "Lucas", "Adam", "Arthur", "Jules", "Hugo", "Paul", "Nathan", "Gabin",
    "Noe", "Victor", "Martin", "Mathis", "Axel", "Leon", "Antoine", "Marius", "Valentin", "Clément", "Baptiste", "Samuel", "Augustin",
    "Emma", "Jade", "Louise", "Alice", "Chloe", "Ines", "Lea", "Rose", "Lena", "Anna", "Ambre", "Julia", "Manon", "Juliette", "Lou",
    "Zoe", "Camille", "Eva", "Agathe", "Jeanne", "Lucie", "Sarah", "Romane", "Charlotte"
};

const std::vector<std::string> Definitions::_lastnames = {
    "Petit", "Durand", "Dubois", "Moreau", "Lefebvre", "Leroy", "Roux", "Morel", "Fournier", "Girard", "Bonnet", "Dupont", "Lambert",
    "Fontaine", "Rousseau", "Muller", "Lefevre", "Faure", "Mercier", "Blanc", "Guerin", "Boyer", "Garnier", "Chavelier", "Legrand",
    "Garcia", "Perrin", "Morin", "Roussel", "Masson", "Marchand", "Duval", "Dumont", "Lemaire", "Meyer", "Dufour", "Meunier", "Brun",
    "Blanchard", "Giraud", "Joly", "Riviere", "Brunet", "Gaillard", "Barbier", "Martinez", "Roche", "Renard", "SChmitt", "Roy",
    "Leroux", "Vidal", "Caron", "Picard", "Fabre", "Lemoine", "Dumas", "Lacroix", "Bourgeois", "Rey", "Lopez", "Dupuy", "Guillot",
    "Berger", "Carpentier", "Sanchez", "Dupuis", "Moulin", "Deschamps", "Huet", "Vasseur", "Perez", "Boucher", "Fleury", "Royer", "Klein",
    "Jacquet", "Paris", "Poirier", "Marty", "Aubry", "Guyot", "Carre", "Renault", "Charpentier", "Menard", "Baron", "Bertin", "Bailly",
    "Schneider", "Fernandez", "Le Gall", "Leger", "Bouvier", "Prevost", "Millet", "Perrot", "Le Roux", "Cousin", "Germain", "Breton",
    "Besson", "Langlois", "Le Goff", "Pelletier", "Leveque", "Perrier", "Leblanc", "Barre", "Lebrun", "Marchal", "Weber", "Mallet",
    "Hamon", "Boulanger", "Jacob", "Monnier", "Michaud", "Rodriguez", "Guichard", "Gillet", "Grondin", "Poulain", "Tessier", "Da Silva",
    "Lemaitre", "Marechal", "Humbert", "Hoarau", "Perret", "Cordier", "Pichon", "Lejeune", "Lamy", "Delaunay", "Pasquier", "Laporte"
};

/******************
 * Class <Animal> *
 ******************/
Definitions::Animal::Animal(int lt, int aa, int ea, int bp, std::string name, Type type):
    _lifetime{lt},
    _adultAge{aa},
    _elderAge{ea},
    _basePrice{bp},
    _name{name},
    _type{type}
{}

/*****************
 * Class <Staff> *
 *****************/
Definitions::Staff::Staff(std::string name, unsigned int experience, Definitions::Staff::Job job):
    _name{name},
    _experience{experience},
    _job{job}
{}

/**
 * @brief Initializes all animals settings data
 */
Definitions::Definitions(){
    _animals.insert(std::make_pair<Animal::Type, Animal>(Animal::Lion, Animal(120, 30, 100, 500, "Lion", Definitions::Animal::Lion)));
    _animals.insert(std::make_pair<Animal::Type, Animal>(Animal::Zebra, Animal(200, 10, 150, 200, "Zèbre", Definitions::Animal::Zebra)));
}

/**
 * @brief Returns a Singleton of class <Definitions>
 * @return Game::Definitions
 */
const Definitions& Definitions::intance(){
    static Definitions _instance;
    return _instance;
}
/**
 * @brief Get an animal randomly from the ones defined
 * @return Game::Definitions::Animal
 */
const Definitions::Animal& Definitions::randomAnimal() const {
    return _animals.at(static_cast<Animal::Type>(Utils::random(Animal::TypeCount - 1)));
}

/**
 * @brief Returns a randomly created new employee fresh to start being a slave !
 * @return Game::Definitions::Staff
 */
Definitions::Staff Definitions::randomStaff() const {
    return Staff(
        _firstnames.at(Utils::random(_firstnames.size() - 1)) + " " + _lastnames.at(Utils::random(_lastnames.size() - 1)),
        Utils::random(1, Staff::MaxExperience/2),
        static_cast<Staff::Job>(Utils::random(Staff::JobCount - 1))
    );
}
