#ifndef DEFS_H
#define DEFS_H

#include <map>
#include <string>
#include <vector>

typedef int Age;

namespace Game {

class Definitions {
public:

    enum GameAction {
        None = 0,

        MainMenu,

        StartZoo,

        LeaveRoom,
        EnterAction,
        BackAction,

        MarketMenu,
        MarketAnimal,
        MarketEmployee,

        ConstructPath,
        ConstructCage,
        ConstructGiftShop,
        ConstructFirstAid,
        ConstructRestaurant,

        Quit
    };

    // Struct because it contains only static members
    struct Map {
        static const int DefaultHeight;
        static const int DefaultWidth;
    };

    // Class <Animal> for static initial definitions
    class Animal {
    public:
        enum Type {
            Lion,
            Zebra,
            TypeCount
        };

        Age lifetime() const { return _lifetime; }
        Age adultAge() const { return _adultAge; }
        Age elderAge() const { return _elderAge; }
        int basePrice() const { return _basePrice; }
        std::string name() const { return _name; }
        Type type() const { return _type; }

    protected:
        Animal(int lt, int aa, int ea, int bp, std::string name, Type type);

        Age _lifetime;
        Age _adultAge;
        Age _elderAge;
        int _basePrice;
        std::string _name;
        Type _type;

        // We trust class <Definitions> to create "new Animals"
        friend class Definitions;

    };

    // Class <Staff> for initial definition data
    class Staff {
    public:
        enum Job {
            Trainer,
            Caretaker,
            ServiceAgent,
            JobCount
        };

        enum Salary {
            TrainerSalary = 150,
            CaretakerSalary = 250,
            ServiceAgentSalary = 100
        };

        enum SalaryFactor {
            NoFactor = 0,
            TrainerFactor = 4,
            CaretakerFactor = 2,
            ServiceAgentFactor = 1
        };

        static const unsigned int MaxExperience;

        std::string name() const { return _name; }
        unsigned int experience() const { return _experience; }
        Job job() const { return _job; }

    private:
        Staff(std::string name, unsigned int experience, Definitions::Staff::Job job);

        std::string _name;
        unsigned int _experience;
        Job _job;

        // We trust class <Definitions> to create "new Animals"
        friend class Definitions;
    };

    static const Definitions& intance();

    const Animal& randomAnimal() const;
    Staff randomStaff() const;

    ~Definitions(){}

private:

    Definitions();

    std::map<Animal::Type, Animal> _animals;        // Definitions of all animals

    // Staff stuff
    static const std::vector<std::string> _firstnames;
    static const std::vector<std::string> _lastnames;
};

}
#endif // DEFS_H
