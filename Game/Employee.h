#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <string>

#include "Object.h"

namespace Game {

class Employee : public Object
{
public:
    Employee(std::string name, unsigned int exp, unsigned int hiredDay);
    virtual ~Employee(){}

    std::string name() const { return _fullname; }
    unsigned int experience() const { return _expericence; }
    unsigned int hiredDay() const { return _hiredDay; }

    void update(const Zoo &z);

private:
    std::string _fullname;
    unsigned int _expericence;
    unsigned int _hiredDay;
};

}

#endif // EMPLOYEE_H
