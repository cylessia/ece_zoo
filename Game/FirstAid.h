#ifndef FIRSTAID_H
#define FIRSTAID_H

#include "Building.h"

namespace Game {

class FirstAid : public Building
{
public:
    FirstAid();
    virtual ~FirstAid(){}

    void update(const Zoo &z);
};

}

#endif // FIRSTAID_H
