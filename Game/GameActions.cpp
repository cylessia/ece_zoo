#include "Zoo.h"
#include "Market.h"
#include "Map.h"

#include "FirstAid.h"
#include "Restaurant.h"
#include "Shop.h"

#include <iostream>

using namespace Game;

void Zoo::actionLeave(){
    while(_currentMenu->parent()){
        _currentMenu = _currentMenu->parent();
    }
    _currentAction = _currentMenu;

    for(auto it = _objects.begin(); it != _objects.end(); ++it){
        delete (*it);
    }
    _objects.clear();
    if(_market){
        delete _market;
        _market = nullptr;
    }
    if(_map){
        delete _map;
        _map = nullptr;
    }

    _isPlaying = false;
}


void Zoo::actionStart() {
    std::cout << "== Nouvelle partie ==" << std::endl;
    std::cout << "Veuillez saisir le nom du zoo : " << std::endl;

    std::cin.ignore();              // Clear the buffer
    std::getline(std::cin, _name);  // std::getline to parse multiple words !

    _market = new Market;
    _map = new Map(Utils::Size(Definitions::Map::DefaultWidth, Definitions::Map::DefaultHeight));
    _isPlaying = true;

    _currentMenu = _currentAction;
}

void Zoo::actionQuit(){
    _run = 0;
}


void Zoo::actionMarketMenu()
{
    if(!_roomBackMenu){
        _roomBackMenu = _currentMenu;
        _currentMenu = _market->menu();
    } else {
        _currentMenu = _currentAction;
    }
}

/// TODO : Think about refactoring those following function because it too much copy/paste...
void Zoo::actionConstructRestaurant() {
    bool redone = false;
    std::list<Utils::Coords> buildingCoordinates;
    do {
        if(redone){
            std::cout << "Erreur, veuillez recommencer !" << std::endl;
        }
        buildingCoordinates = _askPolygonCoordinates("Veuillez entrer les coordonnees du restaurant");
        // Did he cancel ?
        // Does the building has wall that cross any other ?
        if(buildingCoordinates.size() == 0){
            return;
        }
        // Now, check that every item doesn't intersect our new one !
    } while(_map->isAcceptable(buildingCoordinates));
}

void Zoo::actionConstructFirstAid() {
    std::list<Utils::Coords> coordinates = _createValidBuilding();
    if(coordinates.size() == 0){
        return;
    }
    Zoo::_addBuilding(new FirstAid(), coordinates, "black", "purple");
}

void Zoo::actionConstructGiftShop() {
    std::list<Utils::Coords> buildingCoordinates = _askPolygonCoordinates("Veuillez entrer les coordonnees de la cage");
    if(buildingCoordinates.size() == 0) {
        return;
    }
    // Now, check that every item doesn't intersect our new one !
}

void Zoo::actionConstructCage() {
    std::list<Utils::Coords> cageCoordinates = _askPolygonCoordinates("Veuillez entrer les coordonnees de la cage");
    if(cageCoordinates.size() == 0) {
        return;
    }
    // Now, check that every item doesn't intersect our new one !
}

void Zoo::actionConstructPath() {
    //std::list<Utils::Coords> pathCoordinates = _askPathCoordinates("Veuillez entrer les coordonnees du chemin");
    // Now, check that every item doesn't intersect our new paths !
}
