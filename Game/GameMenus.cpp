#include "Zoo.h"

using namespace Game;

void Zoo::_exec(Game::Definitions::GameAction action){
    switch(static_cast<int>(action)){

    /***************************
     * Generic actions of game *
     ***************************/
    case Definitions::MainMenu:  // Main menu (Start / Quit game)
        actionLeave();
        break;
    case Definitions::Quit:  // Quit the game
        actionQuit();
        break;
    case Definitions::StartZoo: // Create a new Zoo
        actionStart();
        break;
    case Definitions::MarketMenu:  // Go to Market Room
        actionMarketMenu();
        break;
    case Definitions::LeaveRoom: // Return to parent "place"
        if(!_roomBackMenu){
            std::cout << "Current room has no back menu" << std::endl;
            std::cout << "Ending game to avoid infinite loop" << std::endl;
            actionQuit();
        }
        _currentMenu = _roomBackMenu;
        _roomBackMenu = nullptr;
        break;
    case Definitions::EnterAction: // Just enter the current menu action
        _currentMenu = _currentAction;
        break;
    case Definitions::BackAction: // Return to parent menu
        if(!_currentMenu->parent()){
            std::cout << "Action has no parent" << std::endl;
            std::cout << "Ending game to avoid infinite loop" << std::endl;
            actionQuit();
        }
        _currentMenu = _currentMenu->parent();
        break;

    /***********************************
     * Construction / Creation actions *
     ***********************************/
    case Definitions::ConstructGiftShop:
        actionConstructGiftShop();
        break;
    case Definitions::ConstructFirstAid:
        actionConstructFirstAid();
        break;
    case Definitions::ConstructRestaurant:
        actionConstructRestaurant();
        break;
    case Definitions::ConstructCage:
        actionConstructCage();
        break;
    case Definitions::ConstructPath:
        actionConstructPath();
        break;

    default:
        std::cout << "Action <" << _currentAction->name() << "> (callback <" << _currentAction->callbackId() << ">) has not been defined." << std::endl;
        std::cout << "Ending game to prevent infinite loop" << std::endl;
        actionQuit();
        break;
    }
}
