#ifndef PATH_H
#define PATH_H

#include <vector>

#include "Object.h"
#include "Svg/Rectangle.h"
#include "Utils/Coords.h"

namespace Game {

class GamePath : public Object
{
public:
    GamePath(Utils::Coords start, Utils::Coords end);
    virtual ~GamePath() {}

    void update(const Zoo &z){}

private:
    Utils::Coords _startCoords;
    Utils::Coords _endCoords;
    Svg::Rectangle _rect; /// Do we need the Rectangle AND the start/end Coords ?
    std::vector<GamePath*> _startSibling;   // GamePath that starts (or ends) at _startCoords
    std::vector<GamePath*> _endSibling;     // GamePath that starts (or ends) at _endCoords

};

}
#endif // PATH_H
