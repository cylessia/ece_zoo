#include "Zoo.h"
#include "Map.h"

#include "Building.h"
#include "Svg/Polygon.h"


#include <queue>
#include <utility> // std::make_pair

using namespace Game;

/// TODO : Think about getting to sleep when we are able to lay this heedless code...

typedef std::pair<const Utils::Coords*, const Utils::Coords*> PairedCoords;

/// TODO : I don't like this code... At all...
/// Must be rewritten
std::list<Utils::Coords> Zoo::_askPolygonCoordinates(std::string message) {

    std::string tmpCoordsInput("");
    std::list<Utils::Coords> coords;
    Utils::Coords tmpCoords(0,0);

    _map->enableGrid(true);

    do  {
        std::cout << message << std::endl;
        std::cout << "Laisser vide pour terminer" << std::endl;
        std::cin.ignore();
        while(true) {
            std::getline(std::cin, tmpCoordsInput);

            if(tmpCoordsInput == ""){
                break;
            }
            std::cout << "Les coordonnees saisies sont : " << tmpCoordsInput << std::endl;
            tmpCoords = Utils::Coords::fromString(tmpCoordsInput);
            if(coords.size() > 0){
                if( tmpCoords == (*(coords.begin())) ) {
                    break;
                }
            }
            coords.push_back(tmpCoords);
        }
        if(_hasIntersections(coords)){
            std::cout << "Les coordonnees ne peuvent pas se croiser." << std::endl;
            coords.clear();
        }
        if(coords.size() < 3) {
                int choice = -1;
                std::cout << "Vous devez saisir au moins 3 coordonnees" << std::endl;
            do {
                std::cout << "Souhaitez-vous recommencer ou annuler ?" << std::endl;
                std::cout << "1. Recommencer" << std::endl;
                std::cout << "0. Annuler" << std::endl;
                std::cin >> choice;
            } while(choice != 0 && choice != 1);
            if(choice == 0){
                coords.clear();
                return coords;
            }
        }
    } while(coords.size() < 3);
    return coords;

}

/// TODO : Ditto... It's awful !
/// Rewrite that...
bool Zoo::_hasIntersections(const std::list<Utils::Coords> &coordList) {

    if(coordList.size() < 4){
        return false;
    }
    std::queue<PairedCoords> queue;
    std::list<PairedCoords> list;
    const Utils::Coords *first = nullptr, *second = nullptr;
    for(auto it = coordList.begin(); it != coordList.end(); ++it) {
        if(!first){
            first = (&(*it));
        } else if(!second){
            second = (&(*it));
        }
        if(first && second){
            queue.push(std::make_pair(first, second));
            list.push_back(std::make_pair(first, second));
            first = second = nullptr;
        }
    }
    if(first && !second){
        queue.push(std::make_pair(first, &(*(coordList.begin()))));
        list.push_back(std::make_pair(first, &(*(coordList.begin()))));
    }
    PairedCoords tmp;
    while(!queue.empty()){
        tmp = queue.front();
        for(auto it = list.begin(); it != list.end(); ++it){
            std::cout << "On verifie " << std::endl;
            std::cout << "Si la ligne (" << tmp.first->x() << "," << tmp.first->y() << ")->(" << tmp.second->x() << "," << tmp.second->y() << ")" << std::endl;
            std::cout << "croise     (" << (*it).first->x() << "," << (*it).first->y() << ")->(" << (*it).second->x() << "," << (*it).second->y() << ")" << std::endl;

            if(
                (tmp.first == (*it).first || tmp.second == (*it).second)
            ){
                std::cout << "On a un point commun, on skip" << std::endl;
                continue;
            }

            if(!(
                (tmp.first->x() < (*it).first->x() && tmp.second->x() < (*it).second->x())
                || (tmp.first->x() > (*it).first->x() && tmp.second->x() > (*it).second->x())
                || (tmp.first->y() < (*it).first->y() && tmp.second->y() < (*it).second->y())
                || (tmp.first->y() > (*it).first->y() && tmp.second->y() > (*it).second->y())
            )){
                std::cout << "On a une intersection !!!!" << std::endl;
                std::cout << "La ligne (" << tmp.first->x() << "," << tmp.first->y() << ")->(" << tmp.second->x() << "," << tmp.second->y() << ")" << std::endl;
                std::cout << "Croise   (" << (*it).first->x() << "," << (*it).first->y() << ")->(" << (*it).second->x() << "," << (*it).second->y() << ")" << std::endl;
                return true;
            }
        }
        queue.pop();
    }
    return false;
}

std::list<Utils::Coords> Zoo::_createValidBuilding() {
    bool valid = true;
    std::list<Utils::Coords> buildingCoordinates;
    do {
        buildingCoordinates = _askPolygonCoordinates("Veuillez entrer les coordonnees de l'infirmerie");
        if(buildingCoordinates.size() == 0) {
            return buildingCoordinates;
        }
        // Now, check that every item doesn't intersect our new one !
        valid = _map->isAcceptable(buildingCoordinates);
    } while(!valid);
    return buildingCoordinates;
}

void Zoo::_addBuilding(Building *building, std::list<Utils::Coords> coords, std::string strokeColor, std::string fillColor){
    Utils::Coords front = coords.front();
    coords.pop_front();
    building->setGraphicItem(new Svg::Polygon(front.x(), front.y(), coords, strokeColor, 1.0, fillColor));
    _objects.push_back(building);
}
