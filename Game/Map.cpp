#include "Map.h"

using namespace Game;

Map::Map(Utils::Size size) :
    _size{size},
    _zooEntry{0.0, 0.0},
    _unitFactor{1.0},
    _enableGrid{false}
{}

void Map::update(){
    /// TODO : Redraw every objects
}

void Map::enableGrid(bool enable){
    // We update the map ONLY when the value
    // is different to optimize the rendering
    if(_enableGrid != enable){
        _enableGrid = enable;
        update();
    }
}

void Map::disableGrid(bool disable){
    enableGrid(!disable);
}
