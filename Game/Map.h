#ifndef MAP_H
#define MAP_H

#include "Object.h"
#include "Svg/GraphicItem.h"
#include "Utils/Coords.h"
#include "Utils/Size.h"

#include <list>

namespace Game {

// The map is not part of the game
// The game can be updated w/o the map
// The map may be re-rendered w/o the game to be updated (display the grid)
class Map
{
public:
    Map(Utils::Size size);
    virtual ~Map() {}

    void update();

    void enableGrid(bool enable);
    void disableGrid(bool disable);
    bool isGridEnabled() const { return _enableGrid; }
    bool isAcceptable(std::list<Utils::Coords> coordinates);

private:
    std::list<Svg::GraphicItem*> _decorations;  // Drawing object with no "physics" (Grass, Tree, ...) Water ?
    std::list<Object*> _objects;                // Game items (must be updated every refresh) Cage -> Animal, GamePath -> Visitors, ...

    Utils::Size _size;
    Utils::Coords _zooEntry;
    // zoom factor
    double _unitFactor;

    bool _enableGrid;
};

}
#endif // MAP_H
