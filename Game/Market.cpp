#include "Market.h"

#include "Utils/Utils.h"
#include "Zoo.h"

#include <string>

using namespace Game;

const int Market::MaxMarketElementCount = 9;

Market::Market()
{
    _currentMenu = new Utils::Action(Game::Definitions::MarketMenu, "Marché");
    _currentMenu->addAction(Game::Definitions::LeaveRoom, "Quitter le marché");
    _currentMenu->addAction(Game::Definitions::MarketMenu, "Acheter un animal");
    _currentMenu->addAction(Game::Definitions::MarketMenu, "Engager un employé");

    _currentMenu->action(1)->addAction(Game::Definitions::BackAction, "Retour au marché");
    _currentMenu->action(2)->addAction(Game::Definitions::BackAction, "Retour au marché");

    _animals.push_back(generate(Definitions::intance().randomAnimal()));
    _staff.push_back(generate(Definitions::intance().randomStaff()));
    _updateMarketMenu();
}

Market::~Market(){
    for(auto it = _animals.begin(); it != _animals.end(); ++it){
        delete (*it);
    }
    _animals.clear();
    // _staff contains values (not pointers) so don't need to be freed
}

void Market::update(const Zoo &z){
    // Update animals and staff list
    bool updated = false;
    bool timeToGenerate = z.time().hour() == 0
                          || z.time().hour() == 6
                          || z.time().hour() == 18;

    if(_animals.size() == 0 || timeToGenerate){
        Animal *animal = generate(Definitions::intance().randomAnimal());
        if(animal){
            _animals.push_back(animal);
            updated = true;
        }
    }

    if(_animals.size() > MaxMarketElementCount){
        _animals.pop_front();
        updated = true;
    }

    if(_staff.size() == 0 || timeToGenerate){
        _staff.push_back(generate(Definitions::intance().randomStaff()));
        updated = true;
    }

    if(_staff.size() > MaxMarketElementCount){
        _animals.pop_front();
        updated = true;
    }

    if(updated){
        _updateMarketMenu();
    }
}

void Market::_updateMarketMenu() {
    Utils::Action* animalAction = _currentMenu->action(1);
    Utils::Action* staffAction = _currentMenu->action(2);

    {
        Utils::VectorAction vec = animalAction->actions();
        for(auto it = vec.begin() + 1; it != vec.end(); ++it) {
            delete (*it);
        }
        for(std::list<Animal*>::iterator it = _animals.begin(); it != _animals.end(); ++it){
            animalAction->addAction(Game::Definitions::MarketAnimal, ((*it)->name()) + " " + std::to_string((*it)->price()));
        }
    }

    {
        Utils::VectorAction vec = staffAction->actions();
        for(auto it = vec.begin() + 1; it != vec.end(); ++it) {
            delete (*it);
        }
        for(std::list<Staff>::iterator it = _staff.begin(); it != _staff.end(); ++it) {
            staffAction->addAction(Game::Definitions::MarketEmployee, ((*it).name()) + " " + std::to_string((*it).price()));
        }
    }
}

Market::Animal* Market::generate(const Game::Definitions::Animal &definition){
    int age = Animal::calculateAge(definition);
    return new Animal(
        age, Animal::calculatePrice(age, definition), definition.name(), definition.type()
    );
}

Market::Staff Market::generate(const Definitions::Staff definition){
    return Staff(definition.name(), definition.experience(), definition.job(), Staff::calculateSalary(definition.experience(), definition.job()));
}

/*************************
 * Internal class <Item> *
 *************************/
Market::Item::Item(int price, std::string name):
    _price{price},
    _name{name}
{}

/***************************
 * Internal class <Animal> *
 ***************************/
Market::Animal::Animal(int age, int price, std::string name, Game::Definitions::Animal::Type type):
    Item(price, name),
    _age{age},
    _type{type}
{}

Age Market::Animal::calculateAge(const Game::Definitions::Animal &animal){
    return Utils::random(animal.adultAge(), animal.elderAge()-animal.adultAge());
}
#include <iostream>
int Market::Animal::calculatePrice(int age, const Game::Definitions::Animal &animal){
    return static_cast<int>(animal.basePrice() * (static_cast<double>(animal.elderAge()) / static_cast<double>(age)));
}

/**************************
 * Internal class <Staff> *
 **************************/
Market::Staff::Staff(std::string name, unsigned int experience, Definitions::Staff::Job job, int price):
    Item(price, name),
    _exp{experience},
    _job{job}
{}

int Market::Staff::calculateSalary(unsigned int experience, Definitions::Staff::Job job){
    int baseSalary = 0;
    int factor = 1;
    switch(job){
    case Definitions::Staff::Caretaker:
        baseSalary = Definitions::Staff::CaretakerSalary;
        factor = Definitions::Staff::CaretakerFactor;
        break;
    case Definitions::Staff::Trainer:
        baseSalary = Definitions::Staff::TrainerSalary;
        factor = Definitions::Staff::TrainerFactor;
        break;
    case Definitions::Staff::ServiceAgent:
    default:
        baseSalary = Definitions::Staff::ServiceAgentSalary;
        factor = Definitions::Staff::ServiceAgentFactor;
        break;
    }
    return static_cast<int>(baseSalary * (1 + ((static_cast<double>(experience) / static_cast<double>(Definitions::Staff::MaxExperience)) * (static_cast<double>(factor) / 2.0))));
}
