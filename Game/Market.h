#ifndef MARKET_H
#define MARKET_H

#include "Object.h"
#include "Defs.h"
#include "Utils/Action.h"

#include <list>
#include <sstream>

namespace Game {

class Animal;
class Employee;
class Zoo;

class Market : public Object
{

    class Item {
    public:
        std::string dump(){
            return static_cast<std::ostringstream&&>(std::ostringstream() << _name << " : Prix : " << _price).str();
        }
        int price(){ return _price; }
        std::string name(){ return _name; }

    protected:
        Item(int price, std::string name);
        virtual ~Item(){}

        int _price;
        std::string _name;

    };

public:

    class Animal : public Item {
    public:
        Animal(int age, int price, std::string name, Game::Definitions::Animal::Type type);

        Game::Definitions::Animal::Type type() const { return _type; }

        static int calculateAge(const Game::Definitions::Animal &animal);
        static int calculatePrice(int age, const Game::Definitions::Animal &animal);

    private:
        int _age;
        Game::Definitions::Animal::Type _type;
    };


    class Staff : public Item {
    public:
        Staff(std::string name, unsigned int experience, Definitions::Staff::Job job, int price);
        static int calculateSalary(unsigned int experience, Definitions::Staff::Job job);
    private:
        unsigned int _exp;
        Definitions::Staff::Job _job;
    };


public:
    Market();
    virtual ~Market();

    std::list<Animal*> animals() { return _animals; }
    std::list<Staff> staff() { return _staff; }
    Utils::Action* menu() { return _currentMenu; }

    void update(const Zoo &z);

private:
    static const int MaxMarketElementCount;

    void _updateMarketMenu();

    static Animal* generate(const Game::Definitions::Animal &definition);
    static Game::Animal* create(const Animal &animal);

    static Staff generate(const Game::Definitions::Staff definition);
    static Game::Employee* create(const Staff &definition);

    std::list<Animal*> _animals;
    std::list<Staff> _staff;
    Utils::Action* _currentMenu;
};

}

#endif // MARKET_H
