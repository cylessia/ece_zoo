#include "Object.h"

using namespace Game;

Object::Object()
    :_graphicItem{nullptr}
{

}

Svg::GraphicItem *Object::setGraphicItem(Svg::GraphicItem *gi)
{
    Svg::GraphicItem* _old = _graphicItem;
    _graphicItem = gi;
    return _old;
}
