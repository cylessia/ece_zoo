#ifndef OBJECT_H
#define OBJECT_H

#include "Svg/GraphicItem.h"

namespace Game {

class Zoo;

class Object
{
public:
    Object();
    virtual ~Object(){}

    Svg::GraphicItem* setGraphicItem(Svg::GraphicItem* gi);
    Svg::GraphicItem* graphicItem() const { return _graphicItem; }

    virtual void update(const Zoo &z) = 0;

private:
    Svg::GraphicItem *_graphicItem;
};

}

#endif // OBJECT_H
