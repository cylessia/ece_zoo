#ifndef RESTAURANT_H
#define RESTAURANT_H

#include "BusinessBuilding.h"

namespace Game {

class Restaurant : public BusinessBuilding
{
public:
    Restaurant();
    virtual ~Restaurant(){}
};

}

#endif // RESTAURANT_H
