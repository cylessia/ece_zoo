#ifndef CLEANINGAGENT_H
#define CLEANINGAGENT_H

#include "Employee.h"

namespace Game {

class ServiceAgent : public Employee
{
public:
    ServiceAgent(std::string name, unsigned int experience, unsigned int hiredDay);
    virtual ~ServiceAgent(){}

private:
};

}
#endif // CLEANINGAGENT_H
