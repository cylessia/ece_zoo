#ifndef SHOP_H
#define SHOP_H

#include "BusinessBuilding.h"

namespace Game {

class Shop : public BusinessBuilding
{
public:
    Shop();
    virtual ~Shop(){}
};

}

#endif // SHOP_H
