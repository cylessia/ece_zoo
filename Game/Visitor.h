#ifndef VISITOR_H
#define VISITOR_H

#include "Object.h"

namespace Game {

class Visitor : public Object
{
public:
    Visitor();
};

}
#endif // VISITOR_H
