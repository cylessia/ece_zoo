#include "Zoo.h"
#include "Map.h"
#include "Market.h"

#include <iostream>
#include <vector>
#include <set>

using namespace Game;

Zoo::Zoo() :
    _run(1),
    _isPlaying(false),
    _currentAction{nullptr},
    _currentMenu{nullptr},
    _roomBackMenu{nullptr},
    _map{nullptr},
    _market{nullptr},
    _time{0}
{

}

Zoo::~Zoo(){
    while(_currentMenu->parent()){
        _currentMenu = _currentMenu->parent();
    }
    delete _currentMenu;
    for(auto it = _objects.begin(); it != _objects.end(); ++it){
        delete (*it);
    }
    if(_market){
        delete _market;
    }
    if(_map){ delete _map; }
}

int Zoo::exec(){
    _init();

    while(_run){
        showMenu();
        _exec(_currentAction->callbackId());
        if(_isPlaying){
            update();
        }
        _currentAction = nullptr;
    }
    return 1;
}

void Zoo::update(){
    ++_time;
    for(auto it = _objects.begin(); it != _objects.end(); ++it){
        (*it)->update(*this);
    }
    _map->update();
    _market->update(*this);
}

void Zoo::_init(){
    // Create actions and menus navigation
    _currentMenu = new Utils::Action(Game::Definitions::MainMenu, "Main Menu");
    _currentMenu->addAction(Game::Definitions::Quit, "Quitter");
    Utils::Action* mainGameMenu = _currentMenu->addAction(Game::Definitions::StartZoo, "Creer un nouveau Zoo");

    mainGameMenu->addAction(Game::Definitions::MainMenu, "Retour au menu principal");
    mainGameMenu->addAction(Game::Definitions::MarketMenu, "Ouvrir le marche");
    Utils::Action* constructionMenu = mainGameMenu->addAction(Game::Definitions::EnterAction, "Construire une infrastructure");

    constructionMenu->addAction(Game::Definitions::BackAction, "Retour");
    constructionMenu->addAction(Game::Definitions::ConstructPath, "Construire un chemin");
    Utils::Action* buildingConsutrctionMenu = constructionMenu->addAction(Game::Definitions::EnterAction, "Construire un batiment");
    constructionMenu->addAction(Game::Definitions::ConstructCage, "Construire une cage");
    //Utils::Action* decorationsAction = constructionMenu->addAction(Game::Definitions::EnterAction, "Construire une decoration");

    buildingConsutrctionMenu->addAction(Game::Definitions::BackAction, "Retour");
    buildingConsutrctionMenu->addAction(Game::Definitions::ConstructGiftShop, "Construire une boutique de souvenir");
    buildingConsutrctionMenu->addAction(Game::Definitions::ConstructFirstAid, "Construire une infirmerie");
    buildingConsutrctionMenu->addAction(Game::Definitions::ConstructRestaurant, "Construire une restaurant");

    _currentAction = _currentMenu;
    _time = Utils::Time();
}

void Zoo::showMenu() {
    std::vector<Utils::Action*> actions = _currentMenu->actions();

    // A menu must at least have "Retour" to move to parent menu
    if(actions.size() < 2){
        std::cout << "Menu " << _currentMenu->name() << " has no defined actions, fixing it to menu \"Nothing\"" << std::endl;
        actionQuit();
    }

    int choice = -1;
    do {
        std::cout << " == " << _currentMenu->name() << " == " << std::endl;

        std::vector<Utils::Action*>::iterator firstActionIt = actions.begin();
        for(auto it = firstActionIt + 1; it != actions.end(); ++it){
            std::cout << (*it)->id() << ". " << (*it)->name() << std::endl;
        }
        std::cout << (*firstActionIt)->id() << ". " << (*firstActionIt)->name() << std::endl;


        std::cin >> choice;
        std::cout << "Votre choix est " << choice << std::endl;
    } while(0 < choice && choice > static_cast<int>(actions.size()));

    _currentAction = _currentMenu->action(static_cast<unsigned int>(choice));
    if(!_currentAction){
        _currentAction = _currentMenu;
    }

    std::cout << "Going to " << _currentAction->name() << std::endl;
}
