#ifndef ZOO_H
#define ZOO_H

#include "Object.h"

#include "Svg/SvgFile.h"
#include "Utils/Action.h"
#include "Utils/Time.h"
#include "Utils/Coords.h"

#include <list>

namespace Game {

class Market;
class Map;
class Building;

class Zoo
{
public:

    Zoo();
    virtual ~Zoo();

    /***************************
     * Main software functions *
     ***************************/
    int exec();
    void update();
    void showMenu();

    /***********************
     * Actions of the game *
     ***********************/
    // "Menu Move"
    void actionLeave();
    void actionStart();
    void actionQuit();

    // Market
    void actionMarketMenu();

    // Construct / Buy
    void actionConstructRestaurant();
    void actionConstructFirstAid();
    void actionConstructGiftShop();
    void actionConstructCage();
    void actionConstructPath();

    /**************************
      * Game getter functions *
      *************************/
    Utils::Time time() const { return _time; }


private:
    /******************
     * Tool functions *
     ******************/
    void _init();                                                           // Init the needed memory data
    void _exec(Game::Definitions::GameAction action);                       // The callback function

    void _addBuilding(Building *building, std::list<Utils::Coords> coords, std::string strokeColor, std::string fillColor);
    std::list<Utils::Coords> _askPolygonCoordinates(std::string message);   // Get polygon coordinates from user input
    bool _hasIntersections(const std::list<Utils::Coords>& coordList);      // Check if there are intersections in a "polygon"
    std::list<Utils::Coords> _createValidBuilding();

    int _run;           // Do we quit the game ?
    bool _isPlaying;    // Do we update the game ?

    Utils::Action* _currentAction; // Chosen menu
    Utils::Action* _currentMenu;   // Current menu to show
    Utils::Action* _roomBackMenu; // Where were we before getting into room ?

    std::string _name;              // Name of the zoo (for further loading/saving data)
    SvgFile _svg;                   // The "screen"

    std::list<Object*>   _objects;  // Objects of the game
    Map                  *_map;     // Rendering data
    Market               *_market;  // Market to buy / hire
    Utils::Time          _time;

};

}
#endif // ZOO_H
