SOURCES += \
        $$PWD/Animal.cpp \
        $$PWD/Animals/Lion.cpp \
        $$PWD/Building.cpp \
        $$PWD/BusinessBuilding.cpp \
        $$PWD/Cage.cpp \
        $$PWD/Caretaker.cpp \
        $$PWD/Defs.cpp \
        $$PWD/Employee.cpp \
        $$PWD/FirstAid.cpp \
    $$PWD/GameActions.cpp \
        $$PWD/GameMenus.cpp \
    $$PWD/GameTools.cpp \
        $$PWD/Market.cpp \
        $$PWD/Object.cpp \
        $$PWD/Restaurant.cpp \
    $$PWD/ServiceAgent.cpp \
        $$PWD/Shop.cpp \
        $$PWD/Trainer.cpp \
    $$PWD/Visitor.cpp \
        $$PWD/Zoo.cpp \
    $$PWD/Map.cpp \
    $$PWD/GamePath.cpp

HEADERS += \
    $$PWD/Animal.h \
    $$PWD/Animals/Lion.h \
    $$PWD/Building.h \
    $$PWD/BusinessBuilding.h \
    $$PWD/Cage.h \
    $$PWD/Caretaker.h \
    $$PWD/Defs.h \
    $$PWD/Employee.h \
    $$PWD/FirstAid.h \
    $$PWD/Market.h \
    $$PWD/Object.h \
    $$PWD/Restaurant.h \
    $$PWD/ServiceAgent.h \
    $$PWD/Shop.h \
    $$PWD/Trainer.h \
    $$PWD/Visitor.h \
    $$PWD/Zoo.h \
    $$PWD/Map.h \
    $$PWD/GamePath.h
