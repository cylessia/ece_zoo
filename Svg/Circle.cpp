#include "Circle.h"
#include "SvgFile.h"

using namespace Svg;

Circle::Circle(double x, double y, double radius, std::string color, double thickness) :
    GraphicItem(x, y, color, thickness), _radius{radius}
{

}

void Circle::paint(SvgFile &file) const{
    file.addCircle(_coords.x(), _coords.y(), _radius, _thickness, _colorStroke);
}

Utils::Rectangle Circle::boundingRect() const {
    return Utils::Rectangle(_coords.x() - _radius, _coords.y() - _radius, _coords.x() + _radius, _coords.y() + _radius);
}
