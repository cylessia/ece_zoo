#ifndef CIRCLE_H
#define CIRCLE_H

#include "Line.h"

namespace Svg {

class Circle : public GraphicItem
{
public:
    Circle(double x, double y, double radius, std::string color, double thickness);
    virtual ~Circle(){}

    void setRadius(double radius);

    double radius() const { return _radius; }

    virtual void paint(SvgFile &file) const;
    Utils::Rectangle boundingRect() const;

protected:
    double _radius;
};

}

#endif // CIRCLE_H
