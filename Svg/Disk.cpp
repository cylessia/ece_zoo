#include "Disk.h"
#include "SvgFile.h"

using namespace Svg;

Disk::Disk(double x, double y, double radius, std::string color, double thickness, std::string colorFill) :
    Circle(x, y, radius, color, thickness), _colorFill{colorFill}
{

}

void Disk::paint(SvgFile &file) const{
    file.addDisk(_coords.x(), _coords.y(), _radius, _colorFill, _thickness, _colorStroke);
}
