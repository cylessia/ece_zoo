#ifndef DISK_H
#define DISK_H

#include <string>

#include "Circle.h"

namespace Svg {

class Disk : public Circle
{
public:
    Disk(double x, double y, double radius, std::string color, double thickness, std::string colorFill);
    virtual ~Disk(){}

    void paint(SvgFile &file) const;

private:
    std::string _colorFill;
};

}

#endif // DISK_H
