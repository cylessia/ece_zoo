#include "GraphicItem.h"

using namespace Svg;

GraphicItem::GraphicItem(double x, double y, std::string color, double thickness) :
    _coords{x, y}, _colorStroke{color}, _thickness{thickness}
{

}
