#ifndef GRAPHICITEM_H
#define GRAPHICITEM_H

#include <string>

#include "Utils/Coords.h"
#include "Utils/UtilsRectangle.h"

class SvgFile;

namespace Svg {

class GraphicItem
{
public:
    GraphicItem(double x, double y, std::string color, double thickness = 0);
    virtual ~GraphicItem() {}

    void setColorStroke(std::string color);

    Utils::Coords coords() const { return _coords; }
    std::string colorStroke() const { return _colorStroke; }
    double thickness() const { return _thickness; }

    virtual void paint(SvgFile &file) const = 0;
    virtual Utils::Rectangle boundingRect() const = 0;

protected:
    Utils::Coords _coords;
    std::string _colorStroke;
    double _thickness;
};

}
#endif // GRAPHICITEM_H
