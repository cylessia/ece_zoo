#include "Line.h"

using namespace Svg;

Line::Line(double x, double y, Utils::Coords coords, std::string color, double thickness) :
    GraphicItem(x, y, color, thickness),
    _coords2{coords}
{

}

Utils::Rectangle Line::boundingRect() const {
    return Utils::Rectangle(_coords, _coords2);
}
