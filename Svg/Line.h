#ifndef LINE_H
#define LINE_H

#include "GraphicItem.h"

namespace Svg {

class Line : public GraphicItem
{
public:
    Line(double x, double y, Utils::Coords coords, std::string color, double thickness);
    virtual ~Line() {}

    Utils::Rectangle boundingRect() const;

private:
    Utils::Coords _coords2;
};

}
#endif // LINE_H
