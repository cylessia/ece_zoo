#include "Utils/Utils.h"

#define BOUNDING_RECT_MACRO() do{ \
    double x1 = _coords.x(); \
    double x2 = _coords.x(); \
    double y1 = _coords.y(); \
    double y2 = _coords.y(); \
    for(std::list<Utils::Coords>::const_iterator it = _points.begin(); it != _points.end(); ++it){ \
        x1 = Utils::min(x1, it->x()); \
        x2 = Utils::max(x2, it->x()); \
        y1 = Utils::min(y1, it->y()); \
        y2 = Utils::max(y2, it->y()); \
    } \
    return Utils::Rectangle(x1, y1, x2, y2); \
}while(false)
