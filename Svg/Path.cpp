#include "Macro.h"
#include "Path.h"

using namespace Svg;

Path::Path(double x, double y, std::list<Utils::Coords> points, std::string color, double thickness) :
    GraphicItem(x, y, color, thickness), _points{points}
{

}

void Path::paint(SvgFile &file) const{

}

Utils::Rectangle Path::boundingRect() const{
    BOUNDING_RECT_MACRO();
}
