#ifndef PATH_H
#define PATH_H

#include <list>

#include "Line.h"
#include "Utils/Coords.h"

namespace Svg {

class Path : public GraphicItem
{
public:
    Path(double x, double y, std::list<Utils::Coords> points, std::string color, double thickness);
    virtual ~Path(){}

    void paint(SvgFile &file) const;
    Utils::Rectangle boundingRect() const;

private:
    std::list<Utils::Coords> _points;
};

}

#endif // PATH_H
