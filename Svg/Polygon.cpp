#include "Polygon.h"
#include "SvgFile.h"

#include "Macro.h"
#include "Utils/Utils.h"

using namespace Svg;

Polygon::Polygon(double x, double y, std::list<Utils::Coords> points, std::string color, double thickness, std::string colorFill) :
    Shape(x, y, color, thickness, colorFill), _points{points}
{

}

void Polygon::paint(SvgFile &file) const {
    file.addPolygon(_coords.x(), _coords.y(), _points, _colorFill, _thickness, _colorStroke);
}

Utils::Rectangle Polygon::boundingRect() const{
    BOUNDING_RECT_MACRO();
}
