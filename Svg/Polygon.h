#ifndef POLYGON_H
#define POLYGON_H

#include <list>

#include "Shape.h"
#include "Utils/Coords.h"

namespace Svg {

class Polygon : public Shape
{
public:
    Polygon(double x, double y, std::list<Utils::Coords> points, std::string color, double thickness, std::string colorFill);
    virtual ~Polygon(){}

    void paint(SvgFile &file) const;
    Utils::Rectangle boundingRect() const;

private:
    std::list<Utils::Coords> _points;
};

}
#endif // POLYGON_H
