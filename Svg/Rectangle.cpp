#include "Rectangle.h"
#include "SvgFile.h"

using namespace Svg;

Rectangle::Rectangle(double x, double y, double width, double height, std::string color, double thickness, std::string colorFill) :
    Shape(x, y, color, thickness, colorFill), _size(width, height)
{

}

void Rectangle::paint(SvgFile &file) const {
    file.addRect(_coords.x(), _coords.y(), _size.width(), _size.height(), _colorFill, _thickness, _colorStroke);
}

Utils::Rectangle Rectangle::boundingRect() const{
    Utils::Rectangle(_coords, _size);
}
