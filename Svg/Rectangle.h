#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Shape.h"
#include "Utils/Size.h"

namespace Svg {

class Rectangle : public Shape
{
public:
    Rectangle(double x, double y, double width, double height, std::string color, double thickness, std::string colorFill);
    virtual ~Rectangle(){}

    void paint(SvgFile &file) const;
    Utils::Rectangle boundingRect() const;

private:
    Utils::Size _size;
};

}
#endif // RECTANGLE_H
