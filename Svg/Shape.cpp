#include "Shape.h"

using namespace Svg;

Shape::Shape(double x, double y, std::string color, double thickness, std::string colorFill) :
    GraphicItem(x, y, color, thickness), _colorFill{colorFill}
{
}

