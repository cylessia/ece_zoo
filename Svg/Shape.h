#ifndef SHAPE_H
#define SHAPE_H

#include "GraphicItem.h"
#include "Utils/Coords.h"

namespace Svg {

class Shape : public GraphicItem
{
public:
    Shape(double x, double y, std::string color, double thickness, std::string colorFill);
    virtual ~Shape(){}

    void setColorFill(std::string color);

    std::string colorFill() const { return _colorFill; }

protected:
    std::string _colorFill;
};

}

#endif // SHAPE_H
