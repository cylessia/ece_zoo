﻿#ifndef SVGFILE_H_INCLUDED
#define SVGFILE_H_INCLUDED

#include <fstream>
#include <list>
#include <set>
#include <string>

#include "Utils/Coords.h"

constexpr char defcol[] = "black";

class SvgFile
{
    public:
        SvgFile(std::string _filename = "output.svg", int _width=1000, int _height=800);
        ~SvgFile();

        void addDisk(double x, double y, double r, std::string colorFill, double thickness, std::string color=defcol);
        void addCircle(double x, double y, double r, double thickness, std::string color=defcol);
        void addTriangle(
            double x1, double y1, double x2, double y2,
            double x3, double y3, std::string colorFill,
            double thickness, std::string colorStroke
        );
        void addTriangle(
            double x1, double y1,
            double x2, double y2,
            double x3, double y3,
            std::string colorFill=defcol
        );
        void addLine(double x1, double y1, double x2, double y2, std::string color=defcol);
        void addCross(double x, double y, double span, std::string color=defcol);

        void addText(double x, double y, std::string text, std::string color=defcol);
        void addText(double x, double y, double val, std::string color=defcol);

        void addGrid(double span=100.0, bool numbering=true, std::string color="lightgrey");

        void addRect(
            double x, double y,
            double width, double height,
            std::string colorFill,
            double thickness, std::string color = defcol
        );
        void addPolygon(
            double x, double y,
            std::list<Utils::Coords> points,
            std::string colorFill,
            double thickness, std::string color = defcol
        );
        void addPolygon(
             double x, double y,
             std::list<Utils::Coords> points,
             double thickness, std::string color = defcol
        );

        static std::string makeRGB(int r, int g, int b);

        /// Type non copiable
        SvgFile(const SvgFile&) = delete;
        SvgFile& operator=(const SvgFile&) = delete;

    private:
        std::string m_filename;
        std::ofstream m_ostrm;
        int m_width;
        int m_height;

        // Pour éviter les ouverture multiples
        static std::set<std::string> s_openfiles;
};

#endif // SVGFILE_H_INCLUDED



