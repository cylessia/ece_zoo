OUT_PWD = ../build/Logic
SOURCES += \
    $$PWD/Circle.cpp \
    $$PWD/Path.cpp \
    $$PWD/Rectangle.cpp \
    $$PWD/Shape.cpp \
    $$PWD/SvgFile.cpp \
    $$PWD/Disk.cpp \
    $$PWD/GraphicItem.cpp \
    $$PWD/Line.cpp \
    $$PWD/Polygon.cpp

HEADERS += \
    $$PWD/Circle.h \
    $$PWD/Path.h \
    $$PWD/Rectangle.h \
    $$PWD/Shape.h \
    $$PWD/SvgFile.h \
    $$PWD/Disk.h \
    $$PWD/GraphicItem.h \
    $$PWD/Line.h \
    $$PWD/Polygon.h \
    $$PWD/Macro.h
