#include "Action.h"
#include "Game/Zoo.h"

using namespace Utils;

Action::Action(Game::Definitions::GameAction action, std::string name, Action *parent)
    :_id(static_cast<unsigned int>(parent ? parent->actions().size() : 0)), _parent(parent), _name(name), _callbackId(action)
{

}

Action::~Action(){
    for(VectorAction::iterator it = _actions.begin(); it != _actions.end(); ++it){
        delete (*it);
    }
    _actions.clear();
}

Action* Action::addAction(Action *a) {
    if(!a){
        return nullptr;
    }

    if(a->_parent){
        a->_parent->action(a->_id);
    }
    a->_parent = this;
    _actions.push_back(a);
    return a;
}

Action* Action::addAction(Game::Definitions::GameAction id, std::string name) {
    _actions.push_back(new Action(id, name, this));
    return _actions.back();
}

void Action::setCallbackId(Game::Definitions::GameAction callback) {
    _callbackId = callback;
}

bool Action::operator<(const Action *_action) {
    return _action->_id < _id;
}

bool Action::operator>(const Action *_action) {
    return _action->_id > _id;
}
