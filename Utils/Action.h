#ifndef ACTION_H
#define ACTION_H

#include <vector>
#include <string>
#include <iostream>

#include "Game/Defs.h"


namespace Utils {

class Action;
typedef std::vector<Action*> VectorAction;

class Action
{
public:
    Action(Game::Definitions::GameAction action, std::string name, Action* parent = nullptr);
    ~Action();

    Action *addAction(Action* action);
    Action *addAction(Game::Definitions::GameAction callbackId, std::string name);

    Action* action(unsigned int id) const { return _actions.at(id); }

    const VectorAction&             actions() const { return _actions; }
    unsigned int                    id() const { return _id; }
    std::string                     name() const { return _name; }
    Game::Definitions::GameAction   callbackId() const { return _callbackId; }
    Action*                         parent() const { return _parent; }

    void setCallbackId(Game::Definitions::GameAction callback);

    bool operator<(const Action *_action);
    bool operator>(const Action *_action);

private:
    unsigned int               _id;
    Action*           _parent;
    std::string       _name;
    VectorAction      _actions;
    Game::Definitions::GameAction _callbackId;
};

}
#endif // ACTION_H
