#include "Coords.h"

using namespace Utils;

Coords::Coords(int x, int y) :
    _x{static_cast<double>(x)},
    _y{static_cast<double>(y)},
    _isValid{true}
{}

Coords::Coords(double x, double y) :
    _x{x},
    _y{y},
    _isValid{true}
{}

Coords::Coords(const Coords &other):
    _x(other._x),
    _y(other._y),
    _isValid{other._isValid}
{}

bool Coords::operator==(const Coords &other) {
    return other._x == _x && other._y == _y;

}

Coords &Coords::operator=(const Coords &other) {
    if(this != &other){
       _x = other._x;
       _y = other._y;
    }
    return *this;
}

Coords Coords::fromString(std::string coordStr) {

    std::size_t commaPos = coordStr.find(',');
    if(
        commaPos == std::string::npos
        || commaPos == 0
        || commaPos == (coordStr.length() - 1)
    ){
        Coords c(0,0);
        c._isValid = false;
        return c;
    }

    // Can / Should we use std::stoi(coordStr, ptr) then "std::stoi(ptr, nullptr)" ?
    try {
        int x = std::stoi(coordStr.substr(0, commaPos));
        int y = std::stoi(coordStr.substr(commaPos + 1));

        return Coords(x,y);

    } catch(std::exception&){}

    Coords c(0,0);
    c._isValid = false;
    return c;
}
