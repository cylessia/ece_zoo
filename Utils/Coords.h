#ifndef COORDS_H
#define COORDS_H

#include "Size.h"

#include <string>

namespace Utils {

class Coords
{
public:
    Coords(int x, int y);
    Coords(double x, double y);
    Coords(const Coords &other);
    virtual ~Coords(){}

    void setX(double x);
    void setY(double y);
    void setCoords(double x, double y);
    void setCoords(const Coords &other);

    double x() const { return _x; }
    double y() const { return _y; }
    bool isValid() const { return _isValid; }

    bool operator==(const Coords &other);
    Coords& operator=(const Coords &other);
    Coords  operator+=(const Coords &other);
    Coords  operator+=(const Size &s);

    static Coords fromString(std::string coordStr);


private:
    double _x;
    double _y;
    bool   _isValid;
};


inline Coords operator+(const Coords &c1, const Coords &c2){
    return Coords(c1.x() + c2.x(), c1.y() + c2.y());
}

inline Coords operator+(const Coords &c, const Size &s){
    return Coords(c.x() + s.width(), c.y() + s.height());
}

inline Coords operator-(const Coords &c, const Size &s){
    return Coords(c.x() - s.width(), c.y() - s.height());
}

inline Coords operator+(const Coords &c1, int vector){
    return Coords(c1.x() + vector, c1.y() + vector);
}

inline bool operator==(const Coords &c1, const Coords &c2){
    return c1.x() == c2.x() && c1.y() == c2.y();
}

}

#endif // COORDS_H
