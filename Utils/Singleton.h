#ifndef SINGLETON_H
#define SINGLETON_H

namespace Utils {

template<class T>
class Singleton
{
public:
    static T& instance() {
        static T _instance;
        return _instance;
    }

protected:
    Singleton(){}
    Singleton(const T&) = delete;
    ~Singleton(){}

    static T* _instance;
};


template<class T>
T* Singleton<T>::_instance = 0;

}

#endif // SINGLETON_H
