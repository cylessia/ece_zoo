#include "Size.h"

#include "Utils.h"

#include <stdexcept>

using namespace Utils;

Size::Size(double w, double h){
    setWidth(w);
    setHeight(h);
}

Size::Size(const Size &other):
    _w(other._w),
    _h(other._h)
{}

void Size::setWidth(double w){
    _w = max(w, 0.0);
}

void Size::setHeight(double h){
    _h = max(h, 0.0);
}

void Size::setSize(double w, double h){
    setWidth(w);
    setHeight(h);
}

void Size::setSize(const Size &s){
    _w = s._w;
    _h = s._h;
}

Size Size::operator+=(const Size &s){
    _w += s._w;
    _h += s._h;
    return *this;
}

Size Size::operator*=(double factor){
    factor = abs(factor);
    _w *= factor;
    _h *= factor;
    return *this;
}

Size Size::operator/=(double factor){
    if(factor == 0.0){
        throw new std::runtime_error("Cannot divide a size by 0");
    }
    factor = abs(factor);
    _w *= factor;
    _h *= factor;
    return *this;
}
