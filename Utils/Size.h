#ifndef SIZE_H
#define SIZE_H

namespace Utils {

class Size
{
public:
    Size(double w = 0, double h = 0);
    Size(const Size &other);
    virtual ~Size() {}

    void setWidth(double w);
    void setHeight(double h);
    void setSize(double w, double h);
    void setSize(const Size &s);

    double height() const { return _h; }
    double width() const { return _w; }

    Size operator+=(const Size &s);
    Size operator*=(double factor);
    Size operator/=(double factor);

private:
    double _w;
    double _h;
};

inline Size operator+(const Size &s1, const Size &s2){
    return Size(s1.width() + s2.width(), s1.height() + s2.height());
}

inline Size operator/(const Size &s, int factor) {
    return Size(s.width() / factor, s.height() / factor);
}

inline Size operator*(const Size &s, int factor){
    return Size(s.width() * factor, s.height() * factor);
}

};

#endif // SIZE_H
