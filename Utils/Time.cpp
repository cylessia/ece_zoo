#include "Time.h"
#include "Utils/Utils.h"

using namespace Utils;

Time::Time():
    _hours(0)
{

}

Time::Time(int hours):
    _hours(abs(hours))
{}

Time::Time(unsigned int hours):
    _hours(hours)
{}

Time::Time(const Time &t):
    _hours{t._hours}
{}

Time Time::operator++() {
    ++_hours;
    return *this;
}

Time Time::operator+=(int time) {
    return operator+=(static_cast<unsigned int>(abs(time)));
}

Time Time::operator+=(unsigned int time) {
    _hours += time;
    return *this;
}

Time Time::operator+=(const Time &t) {
    _hours += t._hours;
    return *this;
}
