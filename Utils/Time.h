#ifndef TIME_H
#define TIME_H

namespace Utils {

class Time
{
public:
    Time();
    Time(int time);
    Time(unsigned int time);
    Time(const Time &t);
    ~Time(){}

    Time operator++();
    Time operator+=(int time);
    Time operator+=(unsigned int time);
    Time operator+=(const Time &t);

    unsigned int time() const { return _hours; }
    unsigned int day()  const { return _hours / 24; }
    unsigned int hour() const { return _hours % 24; }
    bool isTimeDay()    const { return hour() > 7 && hour() < 19; }
    bool isTimeNight()  const { return !isTimeDay(); }

private:
    unsigned int _hours;
};

inline Time operator+(const Time &t1, const Time &t2){
    return Time(t1.time() + t2.time());
}

}

#endif // TIME_H
