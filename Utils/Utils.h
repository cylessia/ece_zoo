#ifndef UTILS_H
#define UTILS_H

#include <random>
#include <ctime>


namespace Utils {

template<typename T>
inline T max(T a, T b) {
    return a > b ? a : b;
}

template<typename T>
inline T min(T a, T b) {
    return a < b ? a : b;
}

inline unsigned int abs(int a) {
    return a < 0 ? static_cast<unsigned int>((-a)) : static_cast<unsigned int>(a);
}

template<typename T>
inline T abs(T a) {
    return a < 0 ? (-a) : a;
}

inline int random(int min, int max) {
    std::srand(static_cast<unsigned int>(std::time(nullptr)));
    return (((static_cast<int>(std::rand())) % (max - min)) + min);
}

inline int random(int max) {
    return random(0, max);
}

}

#endif // UTILS_H
