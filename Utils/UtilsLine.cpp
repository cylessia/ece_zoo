#include "UtilsLine.h"

#include <cmath>
#include <iostream>

using namespace Utils;

char get_line_intersection(float p0_x, float p0_y, float p1_x, float p1_y,
    float p2_x, float p2_y, float p3_x, float p3_y);

Line::Line(double x1, double y1, double x2, double y2) :
    _x1{x1},
    _y1{y1},
    _x2{x2},
    _y2{y2}
{}

bool Line::intersects(const Line &other) const{
    return get_line_intersection(_x1, _y1, _x2, _y2, other._x1, other._y1, other._x2, other._y2) == 1;
}

/*!
 * \see https://stackoverflow.com/a/1968345
 */
char get_line_intersection(float p0_x, float p0_y, float p1_x, float p1_y,
    float p2_x, float p2_y, float p3_x, float p3_y)
{
    float s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

    float s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        // Collision detected

        return 1;
    }

    return 0; // No collision
}
