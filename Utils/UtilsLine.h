#ifndef UTILSLINE_H
#define UTILSLINE_H

namespace Utils {

class Line
{
public:
    Line(double x1, double y1, double x2, double y2);
    virtual ~Line(){}

    bool intersects(const Line &other) const;

private:
    double _x1;
    double _y1;
    double _x2;
    double _y2;
};

}

#endif // UTILSLINE_H
