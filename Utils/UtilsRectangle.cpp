#include "Utils.h"
#include "UtilsRectangle.h"

using namespace Utils;

Rectangle::Rectangle(double x1, double y1, double x2, double y2) :
    _x1{x1},
    _x2{x2},
    _y1{y1},
    _y2{y2}
{}

Rectangle::Rectangle(const Coords &coords, const Size &size) :
    _x1{coords.x()},
    _x2{coords.x() + size.width()},
    _y1{coords.y()},
    _y2{coords.y() + size.height()}
{}

Rectangle::Rectangle(const Coords &coords1, const Coords &coords2) :
    _x1{coords1.x()},
    _x2{coords1.x()},
    _y1{coords2.y()},
    _y2{coords2.y()}
{}

Rectangle Rectangle::boundingRect(const std::list<Coords> &points){
    std::list<Coords>::const_iterator it = points.begin();
    double x1 = it->x();
    double x2 = it->x();
    double y1 = it->y();
    double y2 = it->y();
    for (++it ; it != points.end(); ++it){
        x1 = Utils::min(it->x(), x1);
        x2 = Utils::max(it->x(), x2);
        y1 = Utils::min(it->y(), y1);
        y2 = Utils::max(it->y(), y2);
    }
    return Utils::Rectangle(x1, y1, x2, y2);
}

bool Rectangle::intersects(const Rectangle &other) const {
    return (other._x1 < _x1 && other._x2 < _x1)
           || (other._x1 > _x2 && other._x2 > _x2)
           || (other._y1 < _y1 && other._y2 < _y1)
           || (other._y1 > _y2 && other._y2 > _y1);
}
