#ifndef UTILSRECTANGLE_H
#define UTILSRECTANGLE_H

#include <list>

#include "Utils/Coords.h"

namespace Utils {

class Rectangle
{
public:
    Rectangle(double x1,double y1, double x2, double y2);
    Rectangle(const Coords &coords, const Size &size);
    Rectangle(const Coords &coords1, const Coords &coords2);
    virtual ~Rectangle(){}

    static Rectangle boundingRect(const std::list<Coords> &points);
    bool intersects(const Rectangle &other) const;

private:
    double _x1;
    double _x2;
    double _y1;
    double _y2;
};

}

#endif // UTILSRECTANGLE_H
