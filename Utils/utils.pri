SOURCES += \
    $$PWD/Action.cpp \
    $$PWD/Coords.cpp \
    $$PWD/Singleton.cpp \
    $$PWD/Size.cpp \
    $$PWD/Utils.cpp \
    $$PWD/Time.cpp \
    $$PWD/UtilsRectangle.cpp \
    $$PWD/UtilsLine.cpp

HEADERS += \
    $$PWD/Action.h \
    $$PWD/Coords.h \
    $$PWD/Singleton.h \
    $$PWD/Size.h \
    $$PWD/Utils.h \
    $$PWD/Time.h \
    $$PWD/UtilsRectangle.h \
    $$PWD/UtilsLine.h
