TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle

QT += core gui widgets

SOURCES += \
        main.cpp

include (Game/logic.pri)
include (Svg/svg.pri)
include (Utils/utils.pri)

HEADERS +=
