#include <iostream>

#include "Game/Zoo.h"

using namespace std;

/*
 * Les animaux
 *  - Durée de vie  : Etat de viellesse en jour
 *  - Faim / Soif   : Quantité de nourriture / eau consomme par jour
 *  - Bien etre     : Le bien être des animaux joue sur le bien être des visiteurs
 *  ? - Rarete        : La rareté d'un animal permet d'augementer le nombre de visiteurs
 *
 * Les animaux rares sont plus fragiles (durée de vie plus courtes et plus de chance de tomber malade)
 *
 * Un animal peut tomber malade :
 *  - Selon 2 facteurs :
 *      - Son bien-être
 *      - Sa viellesse
 *
 * Un animal meurt s'il n'est pas nourri sur 2 jours ou s'il est trop vieux
 *
 * Les employes
 *  - Il y a 3 types d'employes :
 *      - Les dresseurs
 *          - Ils nourrissent les animaux
 *          - Si tous les animaux sont nourris, ils leurs font faire des activites pour augmenter leur bien-être
 *          - Ils peuvent gérer un certains nombre d'animaux / jour (dépendant de leur expérience)
 *      - Les soigneurs
 *          - Ils soignent les animaux malades
 *          - Ils permettent aussi aux animaux d'augmenter leur longévité (les animaux ne viellissent pas)
 *          - Ils peuvent gérer un certains nombres d'animaux / jour (dépendant de leur expérience)
 *      - Les agents d'entretien
 *          - Ils nettoient le parc et réparent les cages
 *          - Le parc est salis par les visiteurs, dépendant de leur bien-être
 *          - Les visiteurs salissent plus le parc avec la quantité des restaurants
 *  - Tous les employes ont une duree de travail et partent en retraite
 *
 * Gere-t-on la reproduction des animaux ?
 *
 * Les visiteurs :
 *  - Ils viennent tous les jours
 *  - La quantité de visiteurs dépend de la réputation du zoo (nombre / rarete et bien etre des animaux + proprete du parc) et de sa taille
 *  - Les visiteurs ont un bien être en fonction de la propreté du parc et du bien-être des animaux
 *  - Les visiteurs salissent le parc en fonction de leur bien-etre
 *  - Les depenses dependent de leur bien-etre
 *  - Un visiteur dispose d'une quantité d'argent lors de son arrivée
 *
 * Les batiments
 *  - Il y a 3 types de batiments : Les Restaurant, les boutiques et les infirmeries
 *  - Les batiments permettent de renflouer les caisses (a l'exception des infirmeries)
 *  - Le rendement max journalier du batiment depend de sa taille
 *  - Les restaurants permettent au visiteurs de rester plus longtemps (car ils leur peremttent de dejeuner) !
 *
 *  - Il est possible de renflouer les produits des batiments (medicament, repas, cadeau, nourriture/eau du zoo pour les animaux)
 */
#include "Game/Defs.h"
#include "Svg/Rectangle.h"
#include "Svg/Disk.h"
#include "Svg/Circle.h"
#include "Svg/SvgFile.h"

#include <ctime>

int main()
{

//    SvgFile svg;
//    Svg::Rectangle rect(10.0, 20.0, 50.0, 60.0, "red", 1.0, "black");
//    rect.paint(svg);
//    Svg::Circle circle(100.0, 20.0, 20.0, "blue", 1.0);
//    circle.paint(svg);
//    Svg::Disk disk(100.0, 150.0, 25.0, "green", 3.0, "black");
//    disk.paint(svg);
    Game::Zoo zoo;
    return zoo.exec();
}
